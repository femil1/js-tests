const obj = { name: 'JJ', job: 'Programmer', age: 22 };
const renameKey = { name: 'firstName', job: 'Role' };

for (const key in renameKey) {
    delete Object.assign(obj, { [renameKey[key]]: obj[key] })[key];
}
console.log(obj);

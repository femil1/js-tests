const input = [1, 2, 3]

const combinations = arr => arr.reduce((prev, curr, index) => {
    return prev.concat(prev.map(r => {
        // new elm concat with prev all elm
        return [curr].concat(r)
    }))
}, [[]]);

console.log(combinations(input));
const input = [1, 8, 2, 3, 4, 2, 4, 5]

const uniqueArr = arr => arr.filter(i => arr.indexOf(i) === arr.lastIndexOf(i));
const removeDuplicate = arr => arr.filter((item, i) => arr.indexOf(item) === i)

console.log(uniqueArr(input));
console.log(removeDuplicate(input));
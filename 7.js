const input1 = 'y'
const input2 = 'No'

const checkVal = a => /^(y|yes)$/i.test(a)

console.log(checkVal(input1));
console.log(checkVal(input2));
console.log(checkVal('Y'));
console.log(checkVal('Yes'));
console.log(checkVal('2yes'));
console.log(checkVal('yEs'));
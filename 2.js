const inputString = 'col1,col2\na,b\nc,d'

const stringTo2DArr = (str, divider) => {
    const property = (str.slice(0, str.indexOf('\n'))).split(divider)
    const allData = str.slice(str.indexOf('\n') + 1).split('\n')
    console.log(allData);
    const result = allData.map((data, i) => {
        const values = data.split(divider)

        const pair = property.reduce((obj, title, index) => {
            obj[title] = values[index];
            return obj;
        }, {});
        return pair
    })

    return result
}
console.log(stringTo2DArr(inputString, ','));
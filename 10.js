const input1 = 0;
const input2 = 13;

const changeTimeToLocale = (time) => {
    if (time < 0 || time > 24) return 'Please enter valid time.'
    var zone = time >= 12 ? "PM" : "AM";
    var hours = ((time + 11) % 12 + 1) + zone
    return hours
}

console.log(changeTimeToLocale(0));
const input = [
    { name: 'fred', age: 48 },
    { name: 'barney', age: 46 },
    { name: 'barney', age: 36 },
    { name: 'fred', age: 50 },
    { name: 'fred', age: 40 },
    { name: 'fred', age: 60 },
    { name: 'femil', age: 21 },
    { name: 'bill', age: 40 },
    { name: 'aman', age: 20 },
    { name: 'ayush', age: 10 },
];
const orderedBy = ['name', 'age']
const orders = ['asc', 'desc']

const orderBy = (arr, orderedBy, orders) =>
    arr.sort((a, b) =>
        orderedBy.reduce((prev, curr, i) => {
            if (prev === 0) {
                const [p1, p2] = orders && orders[i] === 'desc' ? [b[curr], a[curr]] : [a[curr], b[curr]];
                // console.log(p1, p2);
                prev = p1 > p2 ? 1 : p1 < p2 ? -1 : 0;
            }
            return prev;
        }, 0)
    );

console.log(orderBy(input, orderedBy, orders));
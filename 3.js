const inputObj = [{ col1: 'a', col2: 'b' }, { col1: 'c', col2: 'd' }]

const ArrToString = (arr) => {
    const allData = []
    const keysArr = Object.keys(arr[0])
    allData.push(keysArr.join(','))


    for (const row of arr) {
        const values = keysArr.map((key, i) => {
            return row[key]
        })
        allData.push(values.join(','));
    }

    return String([...allData].join('\\n'))
}
// ArrToString(inputObj)
console.log(ArrToString(inputObj));
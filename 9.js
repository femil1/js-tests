const input = { a: 1, b: '2', c: 3 };
const removeKey = ['b', 'c']

const removeKeyFromObj = (obj, keys) => {
    keys.map(key => delete obj[key])
}

removeKeyFromObj(input, removeKey)

console.log(input);
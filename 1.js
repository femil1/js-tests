const obj1 = { hair: 'long', beard: true }
const obj2 = { hair: 'long', beard: false }

// false because non-primitive data type [object,array,regEx]
console.log(obj1 === obj2);